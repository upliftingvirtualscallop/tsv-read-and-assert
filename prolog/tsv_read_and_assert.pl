:- module(tsv_read_and_assert, [read_in_and_assert_to_filename/1,
				read_in_and_assert_to_name/2,
				read_in_and_assert_with_list_to_name/2,
				read_in_and_assert_with_list_to_filename/1]).

:- use_module("expanded_string_utils.pl").

% Copyright 2018-2020 ZombieChicken

% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published
% by the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.

% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <https://www.gnu.org/licenses/>.



% The predicates in this file expect input from a .tsv file. All lines
% must contain the same number of fields, and in the case of predicates
% that convert CSV fields into a list, the CSV field *must* be the final
% field. For example (ignore the preceding percentage sign and following
% space), if the following line is stored in foo.tsv:
%
% field1	field2	field3	csv1,csv2,csv3
%
% and were passed through assert_tsv_row_with_csv_field/1, the output
% would be foo/4, where the 4th field would be a list containing
% [csv1,csv2,csv3].

read_in_and_assert_to_name(X,Y) :-
    % Read in a csv/tsv file and assert its contents into
    % a series of predicates given by Y.
    access_file(X,read),
    csv_read_file(X,Rows,[functor(Y)]),
    maplist(assert,Rows).

read_in_and_assert_to_filename(X) :-
    % Read in a csv/tsv file and assert its contents into
    % a series of predicates using the basename of the file
    % as the name of the predicate.
    access_file(X,read),
    file_base_name(X,Filename),
    file_name_extension(FileBaseName,_,Filename),
    csv_read_file(X,Rows,[functor(FileBaseName)]),
    maplist(assert,Rows).

read_in_and_assert_with_list_to_name(X,Y) :-
    % Read in a tsv file and assert its contents into
    % a series of predicates named Y. The last entry in the row
    % is treated as a CSV entry and a list is created to append
    % to the asserted predicate.
    access_file(X,read),
    csv_read_file(X,Rows,[functor(Y)]),
    maplist(assert_tsv_row_with_csv_field,Rows).

read_in_and_assert_with_list_to_filename(X) :-
    % Read in a tsv file and assert its contents into
    % a series of predicates named Y. The last entry in the row
    % is treated as a CSV entry and a list is created to append
    % to the asserted predicate.
    access_file(X,read),
    file_base_name(X,Filename),
    file_name_extension(FileBaseName,_,Filename),
    csv_read_file(X,Rows,[functor(FileBaseName)]),
    maplist(assert_tsv_row_with_csv_field,Rows).

last_or_nothing(List,LastInList) :-
    % Return the last entry in a list, and if it is empty, return
    % an empty list.
    last(List,LastInList);
    LastInList = [].

assert_tsv_row_with_csv_field(InputRow) :-
    % Asserts a TSV row that uses a CSV field for it's final field.
    % The CSV field /must/ be the final field, and is translated
    % into a list.
    InputRow =.. RowAsList,
    last_or_nothing(RowAsList,LastInList),
    drop_last_element_from_list(RowAsList,UsefulTSVValues),
    [TSVHead|TSVTail] = UsefulTSVValues,
    ( LastInList \== [] ->
      listify_csv_input(LastInList,UsefulCSVValues);
      UsefulCSVValues = []),
    ReorderedTSVList = [TSVHead,TSVTail],
    flatten(ReorderedTSVList,FlattenedTSVValues),
    append(FlattenedTSVValues,[UsefulCSVValues],CombinedListForTerm),
    NewTerm =.. CombinedListForTerm,
    assertz(NewTerm).

drop_last_element_from_list(OldList,NewList) :-
    reverse(OldList,ReversedList),
    [_|IntermediaryList] = ReversedList,
    reverse(IntermediaryList,NewList).

listify_csv_input(In,Out) :-
    atom_codes(In,TestString),
    phrase(csv(Intermediary,[separator(0',)]),TestString),
    [I2] = Intermediary,
    I2 =.. I3,
    [_|Out] = I3.
